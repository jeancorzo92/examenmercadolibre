package com.app.paying.select_payment_method;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.paying.Payment;
import com.app.paying.R;
import com.app.paying.select_payment_method.adapters.CardIssuersAdapter;
import com.app.paying.select_payment_method.adapters.PaymentMethodsAdapter;
import com.app.paying.select_payment_method.entities.CardIssuer;
import com.app.paying.select_payment_method.entities.PaymentMethod;
import com.jakewharton.rxbinding2.widget.RxAdapterView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentInformationFragment extends Fragment implements PaymentInformationContract.View {


    @BindView(R.id.payment_method_spinner)
    Spinner paymentMethodSpinner;
    @BindView(R.id.continue_button)
    Button continueButton;
    @BindView(R.id.card_issuer_spinner)
    Spinner cardIssuerSpinner;
    @BindView(R.id.card_issuer_container)
    ConstraintLayout cardIssuerContainer;

    private static final String PAYMENT_EXTRA = "paying.PAYMENT_EXTRA";
    private OnPaymentSelectedListener listener;
    private PaymentInformationContract.Presenter presenter;
    private ProgressDialog progressDialog;
    private boolean paymentMethodHasCardIssuers;

    public static PaymentInformationFragment newInstance(Payment payment) {
        PaymentInformationFragment fragment = new PaymentInformationFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PAYMENT_EXTRA, payment);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_method, container, false);
        ButterKnife.bind(this, view);
        presenter.loadPaymentMethodsFor(getPaymentFromBundle().getAmount());
        displayProgressDialog();
        continueButton.setOnClickListener(view1 -> onContinuePressed());
        return view;
    }

    private void onContinuePressed() {
        if (listener != null) {
            PaymentMethod paymentMethod = getSelectedPaymentMethod();
            CardIssuer cardIssuer = (paymentMethodHasCardIssuers) ? getSelectedCardIssuer() : new CardIssuer();
            listener.onPaymentInformationSelected(paymentMethod, cardIssuer);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPaymentSelectedListener) {
            listener = (OnPaymentSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void setPresenter(PaymentInformationContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void displayPaymentMethods(List<PaymentMethod> paymentMethods) {
        PaymentMethodsAdapter paymentMethodsAdapter = new PaymentMethodsAdapter(getContext(), R.layout.payment_method_spinner_view, paymentMethods);
        paymentMethodSpinner.setAdapter(paymentMethodsAdapter);
        setUpSpinner();
        progressDialog.dismiss();
    }

    @Override
    public String getPaymentMethodID() {
        return getSelectedPaymentMethod().getId();
    }

    @Override
    public void displayCardIssuers(List<CardIssuer> cardIssuers) {
        paymentMethodHasCardIssuers = true;
        cardIssuerContainer.setVisibility(View.VISIBLE);
        CardIssuersAdapter cardIssuersAdapter = new CardIssuersAdapter(getContext(), R.layout.payment_method_spinner_view, cardIssuers);
        cardIssuerSpinner.setAdapter(cardIssuersAdapter);
        progressDialog.dismiss();
    }

    @Override
    public void displayErrorRetrievingPaymentMethods() {
        progressDialog.dismiss();
        Toast.makeText(getContext(), "Error al obtener los medios de pago", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayErrorRetrievingCardIssuers() {
        progressDialog.dismiss();
        Toast.makeText(getContext(), "Error al obtener los emisores de tarjetas", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void noPaymentMethodsFound() {
        progressDialog.dismiss();
        listener.onNoPaymentMethodsFound();
    }

    @Override
    public void noCardIssuersFound() {
        paymentMethodHasCardIssuers = false;
        hideCardIssuerSelection();
        enableContinueButton();
        progressDialog.dismiss();
    }

    private void displayProgressDialog() {
        this.progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
    }

    private void setUpSpinner() {
        RxAdapterView.itemSelections(paymentMethodSpinner)
                .subscribe(integer -> presenter.loadCardIssuers());
    }

    private PaymentMethod getSelectedPaymentMethod() {
        return (PaymentMethod) paymentMethodSpinner.getSelectedItem();
    }

    private CardIssuer getSelectedCardIssuer() {
        return (CardIssuer) cardIssuerSpinner.getSelectedItem();
    }

    private Payment getPaymentFromBundle() {
        return getArguments().getParcelable(PAYMENT_EXTRA);
    }

    private void hideCardIssuerSelection() {
        cardIssuerContainer.setVisibility(View.GONE);
    }

    private void enableContinueButton() {
        continueButton.setEnabled(true);
    }

    public interface OnPaymentSelectedListener {
        void onPaymentInformationSelected(PaymentMethod paymentMethod, CardIssuer cardIssuer);
        void onNoPaymentMethodsFound();
    }
}
