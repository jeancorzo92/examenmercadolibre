package com.app.paying.select_payment_method.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CardIssuer implements Parcelable {

    @SerializedName("thumbnail")
    private String thumbnail = "";
    @SerializedName("name")
    private String name = "";
    @SerializedName("id")
    private String id = "";

    public String getThumbnail() {
        return thumbnail;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.thumbnail);
        dest.writeString(this.name);
        dest.writeString(this.id);
    }

    public CardIssuer() {
    }

    protected CardIssuer(Parcel in) {
        this.thumbnail = in.readString();
        this.name = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<CardIssuer> CREATOR = new Parcelable.Creator<CardIssuer>() {
        @Override
        public CardIssuer createFromParcel(Parcel source) {
            return new CardIssuer(source);
        }

        @Override
        public CardIssuer[] newArray(int size) {
            return new CardIssuer[size];
        }
    };
}
