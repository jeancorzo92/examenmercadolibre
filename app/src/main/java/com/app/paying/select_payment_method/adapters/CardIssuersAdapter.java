package com.app.paying.select_payment_method.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.paying.R;
import com.app.paying.select_payment_method.entities.CardIssuer;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardIssuersAdapter extends ArrayAdapter<CardIssuer> {


    private final List<CardIssuer> cardIssuers;

    public CardIssuersAdapter(@NonNull Context context, int resource, @NonNull List<CardIssuer> cardIssuers) {
        super(context, resource, cardIssuers);
        this.cardIssuers = cardIssuers;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewholder;

        if (convertView == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_issuer_spinner_view, parent, false);
            viewholder = new ViewHolder(view);
            view.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) view.getTag();
        }

        viewholder.bind(cardIssuers.get(position));

        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewholder;

        if (convertView == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_issuer_spinner_dropdown_view, parent, false);
            viewholder = new ViewHolder(view);
            view.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) view.getTag();
        }

        viewholder.bind(cardIssuers.get(position));

        return view;
    }

    class ViewHolder {

        @BindView(R.id.card_issuer_logo)
        SimpleDraweeView cardIssuerLogo;
        @BindView(R.id.card_issuer_name)
        TextView cardIssuerName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void bind(CardIssuer cardIssuer) {
            cardIssuerLogo.setImageURI(cardIssuer.getThumbnail());
            cardIssuerName.setText(cardIssuer.getName());
        }

    }

}
