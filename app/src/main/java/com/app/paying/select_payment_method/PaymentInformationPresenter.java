package com.app.paying.select_payment_method;

import com.annimon.stream.Stream;
import com.app.paying.select_payment_method.entities.PaymentMethod;

import java.math.BigDecimal;
import java.util.List;

public class PaymentInformationPresenter implements PaymentInformationContract.Presenter {

    private static final String CARD_TYPE_ALLOWED = "credit_card";

    private PaymentInformationContract.Interactor interactor;
    private PaymentInformationContract.View view;

    public static PaymentInformationPresenter newInstance() {
        return new PaymentInformationPresenter();
    }

    @Override
    public void loadPaymentMethodsFor(BigDecimal amount) {
        validateView();
        validateInteractor();
        interactor.getPaymentMethods(
                paymentMethods -> {
                    List<PaymentMethod> filteredPaymentMethods = getFilteredPaymentMethods(paymentMethods, amount);
                    if (filteredPaymentMethods.size() > 0) {
                        view.displayPaymentMethods(filteredPaymentMethods);
                    } else {
                        view.noPaymentMethodsFound();
                    }
                },
                throwable -> view.displayErrorRetrievingPaymentMethods());
    }

    @Override
    public void loadCardIssuers() {
        validateView();
        validateInteractor();
        interactor.getCardIssuersForCard(
                view.getPaymentMethodID(),
                cardIssuers -> {
                    if (cardIssuers.size() > 0) {
                        view.displayCardIssuers(cardIssuers);
                    } else {
                        view.noCardIssuersFound();
                    }
                },
                throwable -> view.displayErrorRetrievingCardIssuers());
    }

    @Override
    public void setView(PaymentInformationContract.View view) {
        this.view = view;
    }

    @Override
    public void setInteractor(PaymentInformationContract.Interactor interactor) {
        this.interactor = interactor;
    }

    private void validateView() {
        if (this.interactor == null) {
            throw new RuntimeException("View must be set before using the presenter");
        }
    }

    private void validateInteractor() {
        if (this.interactor == null) {
            throw new RuntimeException("Interactor must be set before using the presenter");
        }
    }

    private List<PaymentMethod> getFilteredPaymentMethods(List<PaymentMethod> paymentMethods, BigDecimal amount) {
        return Stream.of(paymentMethods)
                .filter(paymentMethod -> paymentMethod.getPaymentTypeID().equals(CARD_TYPE_ALLOWED))
                .filter(paymentMethod -> amount.compareTo(new BigDecimal(paymentMethod.getMaxAllowedAmount())) != 1)
                .toList();
    }
}
