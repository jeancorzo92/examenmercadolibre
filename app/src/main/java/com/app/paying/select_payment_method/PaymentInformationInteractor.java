package com.app.paying.select_payment_method;

import com.app.paying.select_payment_method.entities.CardIssuer;
import com.app.paying.select_payment_method.entities.PaymentMethod;
import com.app.paying.service.PaymentInformationService;
import com.app.paying.service.ServiceGenerator;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PaymentInformationInteractor implements PaymentInformationContract.Interactor {

    private final PaymentInformationService paymentInformationService;

    public static PaymentInformationInteractor newInstance() {
        return new PaymentInformationInteractor();
    }

    private PaymentInformationInteractor() {
        paymentInformationService = ServiceGenerator.createService(PaymentInformationService.class);
    }

    @Override
    public void getPaymentMethods(Consumer<List<PaymentMethod>> successConsumer, Consumer<? super Throwable> errorConsumer) {
        paymentInformationService.gePaymentMethods()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer, errorConsumer);
    }

    @Override
    public void getCardIssuersForCard(String paymentMethodID, Consumer<List<CardIssuer>> successConsumer, Consumer<? super Throwable> errorConsumer) {
        paymentInformationService.getCardIssuersForCard(paymentMethodID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer, errorConsumer);
    }
}
