package com.app.paying.select_payment_method;

import com.app.paying.select_payment_method.entities.CardIssuer;
import com.app.paying.select_payment_method.entities.PaymentMethod;

import java.math.BigDecimal;
import java.util.List;

import io.reactivex.functions.Consumer;

interface PaymentInformationContract {
    interface View {
        void setPresenter(Presenter presenter);

        void displayPaymentMethods(List<PaymentMethod> paymentMethods);

        void displayErrorRetrievingPaymentMethods();

        void displayCardIssuers(List<CardIssuer> cardIssuers);

        void displayErrorRetrievingCardIssuers();

        void noCardIssuersFound();

        String getPaymentMethodID();

        void noPaymentMethodsFound();
    }

    interface Presenter {

        void setView(View view);

        void setInteractor(Interactor interactor);

        void loadPaymentMethodsFor(BigDecimal amount);

        void loadCardIssuers();
    }

    interface Interactor {
        void getPaymentMethods(Consumer<List<PaymentMethod>> successConsumer, Consumer<? super Throwable> errorConsumer);

        void getCardIssuersForCard(String paymentMethodID, Consumer<List<CardIssuer>> successConsumer, Consumer<? super Throwable> errorConsumer);
    }
}
