package com.app.paying.select_payment_method.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.paying.R;
import com.app.paying.select_payment_method.entities.PaymentMethod;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentMethodsAdapter extends ArrayAdapter<PaymentMethod> {


    private final List<PaymentMethod> paymentMethods;

    public PaymentMethodsAdapter(@NonNull Context context, int resource, @NonNull List<PaymentMethod> paymentMethods) {
        super(context, resource, paymentMethods);
        this.paymentMethods = paymentMethods;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewholder;

        if (convertView == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_method_spinner_view, parent, false);
            viewholder = new ViewHolder(view);
            view.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) view.getTag();
        }

        viewholder.bind(paymentMethods.get(position));

        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewholder;

        if (convertView == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_method_spinner_dropdown_view, parent, false);
            viewholder = new ViewHolder(view);
            view.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) view.getTag();
        }

        viewholder.bind(paymentMethods.get(position));

        return view;
    }

    class ViewHolder {

        @BindView(R.id.payment_method_logo)
        SimpleDraweeView paymentMethodLogo;
        @BindView(R.id.payment_method_name)
        TextView paymentMethodName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void bind(PaymentMethod paymentMethod) {
            paymentMethodLogo.setImageURI(paymentMethod.getThumbnail());
            paymentMethodName.setText(paymentMethod.getName());
        }

    }

}
