package com.app.paying.select_payment_method.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PaymentMethod implements Parcelable {

    @SerializedName("payment_type_id")
    private String paymentTypeID;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private String id;
    @SerializedName("max_allowed_amount")
    private String maxAllowedAmount;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getPaymentTypeID() {
        return paymentTypeID;
    }

    public String getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.paymentTypeID);
        dest.writeString(this.thumbnail);
        dest.writeString(this.name);
        dest.writeString(this.id);
    }

    protected PaymentMethod(Parcel in) {
        this.paymentTypeID = in.readString();
        this.thumbnail = in.readString();
        this.name = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<PaymentMethod> CREATOR = new Parcelable.Creator<PaymentMethod>() {
        @Override
        public PaymentMethod createFromParcel(Parcel source) {
            return new PaymentMethod(source);
        }

        @Override
        public PaymentMethod[] newArray(int size) {
            return new PaymentMethod[size];
        }
    };
}
