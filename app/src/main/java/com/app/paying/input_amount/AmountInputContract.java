package com.app.paying.input_amount;

import java.math.BigDecimal;

interface AmountInputContract {

    interface View {
        void setPresenter(Presenter presenter);

        void setContinueEnabled(boolean enabled);

        String getAmountEntered();
    }

    interface Presenter {
        void setView(View view);

        void validateAmountEntered();

        BigDecimal getAmountValue();
    }

}
