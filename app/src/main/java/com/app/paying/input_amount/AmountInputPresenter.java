package com.app.paying.input_amount;

import java.math.BigDecimal;

public class AmountInputPresenter implements AmountInputContract.Presenter {

    private AmountInputContract.View view;

    public static AmountInputPresenter newInstance() {
        return new AmountInputPresenter();
    }

    @Override
    public void setView(AmountInputContract.View view) {
        this.view = view;
    }

    @Override
    public void validateAmountEntered() {
        validateView();
        String amount = view.getAmountEntered();
        String amountNoSymbols = amount.replaceAll("\\.,", "");
        view.setContinueEnabled(!amountNoSymbols.isEmpty() && isGreaterThanZero(amountNoSymbols));
    }

    @Override
    public BigDecimal getAmountValue() {
        validateView();
        String amount = view.getAmountEntered();
        return new BigDecimal(amount);
    }

    private boolean isGreaterThanZero(String amount) {
        return new BigDecimal(amount).compareTo(BigDecimal.ZERO) > 0;
    }

    private void validateView() {
        if (this.view == null) {
            throw new RuntimeException("View must be set before using the presenter");
        }
    }
}
