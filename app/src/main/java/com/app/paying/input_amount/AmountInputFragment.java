package com.app.paying.input_amount;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.app.paying.R;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class AmountInputFragment extends Fragment implements AmountInputContract.View {

    @BindView(R.id.amount_input)
    EditText amountInput;
    @BindView(R.id.continue_button)
    Button continueButton;
    private Unbinder unbinder;
    private OnAmountConfirmed listener;
    private AmountInputContract.Presenter presenter;

    public static AmountInputFragment newInstance() {
        return new AmountInputFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ammount_input, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpContinueButton();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAmountConfirmed) {
            listener = (OnAmountConfirmed) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setPresenter(AmountInputContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public String getAmountEntered() {
        return amountInput.getText().toString();
    }

    @Override
    public void setContinueEnabled(boolean enabled) {
        continueButton.setEnabled(enabled);
    }

    private void setUpContinueButton() {
        RxTextView.textChanges(amountInput)
                .subscribe(cs -> presenter.validateAmountEntered());
        continueButton.setOnClickListener(v -> onContinueClicked());
    }

    private void onContinueClicked() {
        if (listener != null) {
            listener.onAmountConfirmed(presenter.getAmountValue());
        }
    }

    public interface OnAmountConfirmed {
        void onAmountConfirmed(BigDecimal amount);
    }
}
