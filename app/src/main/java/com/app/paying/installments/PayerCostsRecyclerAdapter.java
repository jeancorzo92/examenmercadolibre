package com.app.paying.installments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.app.paying.R;
import com.app.paying.installments.entities.PayerCost;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class PayerCostsRecyclerAdapter extends RecyclerView.Adapter<PayerCostsRecyclerAdapter.ViewHolder> {

    private final List<PayerCost> payerCosts;
    private final OnInstallmentsSelectedListener listener;
    private int lastSelectedPosition = -1;

    PayerCostsRecyclerAdapter(List<PayerCost> payerCosts, OnInstallmentsSelectedListener listener) {
        this.payerCosts = payerCosts;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.installments_recycler_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(payerCosts.get(position), position);
    }

    @Override
    public int getItemCount() {
        return payerCosts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.radioButton)
        RadioButton radioButton;
        @BindView(R.id.recommended_message)
        TextView recommendedMessage;
        @BindView(R.id.fees)
        TextView fees;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(PayerCost payerCost, int position) {
            radioButton.setChecked(lastSelectedPosition == position);
            recommendedMessage.setText(formatRecommendedMessage(payerCost.getRecommendedMessage()));
            fees.setText(getPayerFeesText(payerCost.getLabels()));
            radioButton.setOnClickListener(v -> {
                checkSelectionState();
                listener.onInstallmentsSelected(payerCost);
            });
        }

        private String formatRecommendedMessage(String recommendedMessage) {
            return recommendedMessage.replaceAll("\\$\\s", "\\$");
        }

        private String getPayerFeesText(String[] labels) {
            String feesText = Stream.of(labels)
                    .filter(value -> value.contains("CFT") || value.contains("TEA"))
                    .toList().get(0);
            return formatPayerFeesText(feesText);
        }

        private String formatPayerFeesText(String fees) {
            return  fees.replaceAll("_", " ");
        }

        private void checkSelectionState() {
            lastSelectedPosition = getAdapterPosition();
            notifyDataSetChanged();
        }
    }

    interface OnInstallmentsSelectedListener {
        void onInstallmentsSelected(PayerCost payerCost);
    }
}
