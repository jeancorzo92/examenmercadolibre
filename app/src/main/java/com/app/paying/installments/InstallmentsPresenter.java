package com.app.paying.installments;

import com.app.paying.Payment;

public class InstallmentsPresenter implements InstallmentsContract.Presenter {

    private InstallmentsContract.Interactor interactor;
    private InstallmentsContract.View view;

    public static InstallmentsPresenter newInstance() {
        return new InstallmentsPresenter();
    }

    @Override
    public void loadPayerCosts(Payment payment) {
        validateView();
        validateInteractor();
        interactor.getPayerCosts(payment.getPaymentMethodID(), payment.getAmount().toString(), payment.getCardIssuerID(),
                installments -> view.displayPayerCosts(installments.get(0).getPayerCosts()),
                throwable -> view.displayErrorRetrievingInstallments());
    }

    @Override
    public void setView(InstallmentsContract.View view) {
        this.view = view;
    }

    @Override
    public void setInteractor(InstallmentsContract.Interactor interactor) {
        this.interactor = interactor;
    }

    private void validateView() {
        if (this.interactor == null) {
            throw new RuntimeException("View must be set before using the presenter");
        }
    }

    private void validateInteractor() {
        if (this.interactor == null) {
            throw new RuntimeException("Interactor must be set before using the presenter");
        }
    }
}
