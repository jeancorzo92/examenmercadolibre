package com.app.paying.installments.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Installment {
    @SerializedName("payer_costs")
    private List<PayerCost> payerCosts;

    public List<PayerCost> getPayerCosts() {
        return payerCosts;
    }
}
