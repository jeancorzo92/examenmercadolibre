package com.app.paying.installments;

import com.app.paying.Payment;
import com.app.paying.installments.entities.Installment;
import com.app.paying.installments.entities.PayerCost;

import java.util.List;

import io.reactivex.functions.Consumer;

interface InstallmentsContract {

    interface View {
        void setPresenter(Presenter presenter);

        void displayPayerCosts(List<PayerCost> payerCosts);

        void displayErrorRetrievingInstallments();
    }

    interface Presenter {

        void loadPayerCosts(Payment payment);

        void setView(View view);

        void setInteractor(Interactor interactor);

    }

    interface Interactor {
        void getPayerCosts(String paymentMethodID, String amount, String cardIssuerID, Consumer<List<Installment>> successConsumer, Consumer<? super Throwable> errorConsumer);
    }

}
