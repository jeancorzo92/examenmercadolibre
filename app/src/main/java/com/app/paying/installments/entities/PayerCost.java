package com.app.paying.installments.entities;

import com.google.gson.annotations.SerializedName;

public class PayerCost {

    @SerializedName("recommended_message")
    private String recommendedMessage;
    @SerializedName("labels")
    private String[] labels;

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public String[] getLabels() {
        return labels;
    }
}
