package com.app.paying.installments;

import com.app.paying.installments.entities.Installment;
import com.app.paying.service.InstallmentsService;
import com.app.paying.service.ServiceGenerator;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class InstallmentsInteractor implements InstallmentsContract.Interactor {

    private final InstallmentsService installmentsService;

    public static InstallmentsInteractor newInstance() {
        return new InstallmentsInteractor();
    }

    private InstallmentsInteractor() {
        this.installmentsService = ServiceGenerator.createService(InstallmentsService.class);
    }

    @Override
    public void getPayerCosts(String paymentMethodID, String amount, String cardIssuerID, Consumer<List<Installment>> successConsumer, Consumer<? super Throwable> errorConsumer) {
        installmentsService.getInstallments(paymentMethodID, amount, cardIssuerID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer, errorConsumer);
    }
}
