package com.app.paying.installments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.app.paying.Payment;
import com.app.paying.R;
import com.app.paying.installments.entities.PayerCost;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InstallmentsFragment extends Fragment implements InstallmentsContract.View, PayerCostsRecyclerAdapter.OnInstallmentsSelectedListener {

    private static final String PAYMENT_EXTRA = "paying.PAYMENT_EXTRA";
    @BindView(R.id.payer_costs_recycler)
    RecyclerView payerCostsRecycler;
    @BindView(R.id.continue_button)
    Button continueButton;

    private OnPayerCostSelectionListener listener;
    private InstallmentsContract.Presenter presenter;
    private ProgressDialog progressDialog;
    private PayerCost selectedPayerCost;

    public static InstallmentsFragment newInstance(Payment payment) {
        InstallmentsFragment fragment = new InstallmentsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PAYMENT_EXTRA, payment);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_installments, container, false);
        ButterKnife.bind(this, view);
        displayProgressDialog();
        presenter.loadPayerCosts(getPaymentFromBundle());
        continueButton.setOnClickListener(v -> onContinueButtonPressed());
        return view;
    }

    private void onContinueButtonPressed() {
        if (listener != null) {
            listener.onPayerCostSelected(selectedPayerCost);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPayerCostSelectionListener) {
            listener = (OnPayerCostSelectionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void setPresenter(InstallmentsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void displayPayerCosts(List<PayerCost> payerCosts) {
        progressDialog.dismiss();
        PayerCostsRecyclerAdapter payerCostsRecyclerAdapter = new PayerCostsRecyclerAdapter(payerCosts, this);
        payerCostsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        payerCostsRecycler.setAdapter(payerCostsRecyclerAdapter);
    }

    @Override
    public void displayErrorRetrievingInstallments() {
        progressDialog.dismiss();
        Toast.makeText(getContext(), "Error al obtener los emisores de tarjetas", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInstallmentsSelected(PayerCost payerCost) {
        continueButton.setEnabled(true);
        this.selectedPayerCost = payerCost;
    }

    private void displayProgressDialog() {
        this.progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
    }

    private Payment getPaymentFromBundle() {
        return getArguments().getParcelable(PAYMENT_EXTRA);
    }

    public interface OnPayerCostSelectionListener {
        void onPayerCostSelected(PayerCost payerCost);
    }
}
