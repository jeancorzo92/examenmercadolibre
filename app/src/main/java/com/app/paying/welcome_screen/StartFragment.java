package com.app.paying.welcome_screen;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.paying.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class StartFragment extends Fragment {

    @BindView(R.id.begin_button)
    Button beginButton;
    Unbinder unbinder;
    private OnPaymentStartedListener listener;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);
        unbinder = ButterKnife.bind(this, view);
        beginButton.setOnClickListener(v -> onStartButtonClicked());
        return view;
    }

    public void onStartButtonClicked() {
        if (listener != null) {
            listener.onPaymentStarted();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPaymentStartedListener) {
            listener = (OnPaymentStartedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface OnPaymentStartedListener {
        void onPaymentStarted();
    }
}
