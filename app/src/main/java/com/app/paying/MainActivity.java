package com.app.paying;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.app.paying.input_amount.AmountInputFragment;
import com.app.paying.input_amount.AmountInputPresenter;
import com.app.paying.installments.InstallmentsFragment;
import com.app.paying.installments.InstallmentsInteractor;
import com.app.paying.installments.InstallmentsPresenter;
import com.app.paying.installments.entities.PayerCost;
import com.app.paying.select_payment_method.PaymentInformationFragment;
import com.app.paying.select_payment_method.PaymentInformationInteractor;
import com.app.paying.select_payment_method.PaymentInformationPresenter;
import com.app.paying.select_payment_method.entities.CardIssuer;
import com.app.paying.select_payment_method.entities.PaymentMethod;
import com.app.paying.welcome_screen.StartFragment;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity
        implements StartFragment.OnPaymentStartedListener,
        AmountInputFragment.OnAmountConfirmed,
        PaymentInformationFragment.OnPaymentSelectedListener,
        InstallmentsFragment.OnPayerCostSelectionListener {

    private Payment payment = new Payment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeLibraries();
        loadStartingFragment();
    }

    private void initializeLibraries() {
        Stetho.initializeWithDefaults(this);
        Fresco.initialize(this);
    }

    @Override
    public void onPaymentStarted() {
        loadAmountInputFragment();
    }

    @Override
    public void onAmountConfirmed(BigDecimal amount) {
        payment.setAmount(amount);
        ActivityUtils.hideKeyboard(this);
        loadPaymentInformationFragment();
    }

    @Override
    public void onPaymentInformationSelected(PaymentMethod paymentMethod, CardIssuer cardIssuer) {
        payment.setPaymentMethod(paymentMethod);
        payment.setCardIssuer(cardIssuer);
        loadInstallmentsFragment();
    }

    @Override
    public void onNoPaymentMethodsFound() {
        displayNoPaymentMethodsFound();
        returnToPreviousFragment();
    }

    @Override
    public void onPayerCostSelected(PayerCost payerCost) {
        payment.setPayerCost(payerCost);
        displayQuerySummary();
        returnToStartFragment();
    }

    @Override
    public void onBackPressed() {
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            finish();
        } else {
            if (getFragmentManager().getBackStackEntryCount() > 1) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    private void loadStartingFragment() {
        ActivityUtils.loadFragment(getSupportFragmentManager(), R.id.fragment_container, StartFragment.newInstance());
    }

    private void loadAmountInputFragment() {
        AmountInputFragment fragment = AmountInputFragment.newInstance();
        AmountInputPresenter presenter = AmountInputPresenter.newInstance();
        fragment.setPresenter(presenter);
        presenter.setView(fragment);
        ActivityUtils.loadFragment(getSupportFragmentManager(), R.id.fragment_container, fragment);
    }

    private void loadPaymentInformationFragment() {
        PaymentInformationFragment fragment = PaymentInformationFragment.newInstance(payment);
        PaymentInformationPresenter presenter = PaymentInformationPresenter.newInstance();
        PaymentInformationInteractor interactor = PaymentInformationInteractor.newInstance();
        fragment.setPresenter(presenter);
        presenter.setView(fragment);
        presenter.setInteractor(interactor);
        ActivityUtils.loadFragment(getSupportFragmentManager(), R.id.fragment_container, fragment);
    }

    private void displayNoPaymentMethodsFound() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.no_payment_methods_found_title)
                .setMessage(R.string.no_payment_methods_found_message)
                .setNeutralButton(R.string.aceptar, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    private void returnToPreviousFragment() {
        getSupportFragmentManager().popBackStack();
    }

    private void loadInstallmentsFragment() {
        InstallmentsFragment fragment = InstallmentsFragment.newInstance(payment);
        InstallmentsPresenter presenter = InstallmentsPresenter.newInstance();
        InstallmentsInteractor interactor = InstallmentsInteractor.newInstance();
        fragment.setPresenter(presenter);
        presenter.setView(fragment);
        presenter.setInteractor(interactor);
        ActivityUtils.loadFragment(getSupportFragmentManager(), R.id.fragment_container, fragment);
    }

    private void displayQuerySummary() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.query_summary_title)
                .setMessage(getQuerySummaryMessage())
                .setNeutralButton(R.string.aceptar, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    private String getQuerySummaryMessage() {
        if (payment.getCardIssuerID().isEmpty()) {
            return getString(R.string.query_summary_message_no_issuer, payment.getAmount(), payment.getPaymentMethodName(), payment.getPayerCostMessage());
        } else {
            return getString(R.string.query_summary_message_with_issuer, payment.getAmount(), payment.getPaymentMethodName(), payment.getCardIssuerName(), payment.getPayerCostMessage());
        }
    }

    private void returnToStartFragment() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (!(fragment instanceof StartFragment)) {
                getSupportFragmentManager().popBackStack();
            }
        }
    }
}
