package com.app.paying;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

class ActivityUtils {

    static void loadFragment(FragmentManager fragmentManager, @IdRes int fragmentContainer, Fragment fragment) {
        fragmentManager.beginTransaction()
                .add(fragmentContainer, fragment)
                .addToBackStack(null)
                .commit();
    }

    static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
