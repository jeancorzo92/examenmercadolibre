package com.app.paying;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.paying.installments.entities.PayerCost;
import com.app.paying.select_payment_method.entities.CardIssuer;
import com.app.paying.select_payment_method.entities.PaymentMethod;

import java.math.BigDecimal;

public class Payment implements Parcelable {

    private BigDecimal amount = BigDecimal.ZERO;
    private PaymentMethod paymentMethod;
    private CardIssuer cardIssuer;
    private PayerCost payerCost;

    public BigDecimal getAmount() {
        return amount;
    }

    void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    void setCardIssuer(CardIssuer cardIssuer) {
        this.cardIssuer = cardIssuer;
    }

    void setPayerCost(PayerCost payerCost) {
        this.payerCost = payerCost;
    }

    public String getPaymentMethodID() {
        return paymentMethod.getId();
    }

    String getPaymentMethodName() {
        return paymentMethod.getName();
    }

    public String getCardIssuerID() {
        return cardIssuer.getId();
    }

    String getCardIssuerName() {
        return cardIssuer.getName();
    }

    String getPayerCostMessage() {
        return payerCost.getRecommendedMessage();
    }

    Payment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.amount);
        dest.writeParcelable(this.paymentMethod, flags);
        dest.writeParcelable(this.cardIssuer, flags);
    }

    protected Payment(Parcel in) {
        this.amount = (BigDecimal) in.readSerializable();
        this.paymentMethod = in.readParcelable(PaymentMethod.class.getClassLoader());
        this.cardIssuer = in.readParcelable(CardIssuer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Payment> CREATOR = new Parcelable.Creator<Payment>() {
        @Override
        public Payment createFromParcel(Parcel source) {
            return new Payment(source);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };
}
