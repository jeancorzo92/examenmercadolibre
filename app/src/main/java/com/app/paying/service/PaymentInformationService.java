package com.app.paying.service;

import com.app.paying.select_payment_method.entities.CardIssuer;
import com.app.paying.select_payment_method.entities.PaymentMethod;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PaymentInformationService {

    @GET("payment_methods")
    Single<List<PaymentMethod>> gePaymentMethods();

    @GET("payment_methods/card_issuers")
    Single<List<CardIssuer>> getCardIssuersForCard(@Query("payment_method_id") String paymentMethodID);

}
