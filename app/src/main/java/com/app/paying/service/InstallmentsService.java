package com.app.paying.service;

import com.app.paying.installments.entities.Installment;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InstallmentsService {
    @GET("payment_methods/installments")
    Single<List<Installment>> getInstallments(@Query("payment_method_id") String paymentMethodID, @Query("amount") String amount, @Query("issuer.id") String issuerID);
}
